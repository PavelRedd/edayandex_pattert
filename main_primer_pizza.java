import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        PizzaFactory pizzaFactory = new PizzaFactory();
        Pizza pizza = pizzaFactory.createPizza();

        DeliveryDecorator decoratedPizza = new DeliveryDecorator(pizza);
        decoratedPizza.addDeliveryAddress("Адрес 1");
        decoratedPizza.prepare();

        Order order = new Order();
        Customer customer1 = new Customer("John");
        Customer customer2 = new Customer("Alice");
        order.registerObserver(customer1);
        order.registerObserver(customer2);

        order.placeOrder();
    }
}

abstract class Pizza {
    public void prepare() {
        System.out.println("Pizza is being prepared...");
    }
}

class ConcretePizza extends Pizza {
    @Override
    public void prepare() {
        System.out.println("ConcretePizza is being prepared...");
    }
}

class PizzaFactory {
    public Pizza createPizza() {
        return new ConcretePizza();
    }
}

class DeliveryDecorator extends Pizza {
    private Pizza pizza;

    public DeliveryDecorator(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public void prepare() {
        pizza.prepare();
        System.out.println("Adding delivery service to the pizza...");
    }

    public void addDeliveryAddress(String address) {
        System.out.println("Adding delivery address: " + address);
    }
}

class Order {
    private List<Customer> customers;

    public Order() {
        customers = new ArrayList<>();
    }

    public void registerObserver(Customer customer) {
        customers.add(customer);
    }

    public void placeOrder() {
        System.out.println("Placing order...");

        for (Customer customer : customers) {
            customer.notifyObserver();
        }
    }
}

class Customer {
    private String name;

    public Customer(String name) {
        this.name = name;
    }

    public void notifyObserver() {
        System.out.println("Customer " + name + ": Received notification about order status.");
    }
}
