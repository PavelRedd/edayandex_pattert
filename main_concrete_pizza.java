import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        PizzaFactory factory = new ConcretePizzaFactory();
        Pizza pizza = factory.createPizza();

        PizzaDecorator decoratedPizza = new BasePizzaDecorator(pizza);
        decoratedPizza = new ExtraCheeseDecorator(decoratedPizza);
        decoratedPizza.prepare();

        Order order = new Order();
        Observer customer1 = new Customer("John");
        Observer customer2 = new Customer("Alice");
        order.registerObserver(customer1);
        order.registerObserver(customer2);

        order.placeOrder();
    }
}

abstract class Pizza {
    public abstract void prepare();
}

class ConcretePizza extends Pizza {
    @Override
    public void prepare() {
        System.out.println("ConcretePizza is being prepared...");
    }
}

interface PizzaFactory {
    Pizza createPizza();
}

class ConcretePizzaFactory implements PizzaFactory {
    @Override
    public Pizza createPizza() {
        return new ConcretePizza();
    }
}

abstract class PizzaDecorator extends Pizza {
    protected Pizza pizza;

    public PizzaDecorator(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public void prepare() {
        pizza.prepare();
    }
}

class BasePizzaDecorator extends PizzaDecorator {
    public BasePizzaDecorator(Pizza pizza) {
        super(pizza);
    }

    @Override
    public void prepare() {
        super.prepare();
    }
}

class ExtraCheeseDecorator extends PizzaDecorator {
    public ExtraCheeseDecorator(Pizza pizza) {
        super(pizza);
    }

    @Override
    public void prepare() {
        super.prepare();
        System.out.println("Adding extra cheese to the pizza...");
    }
}

class Order {
    private List<Observer> observers;

    public Order() {
        observers = new ArrayList<>();
    }

    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    public void placeOrder() {
        System.out.println("Placing order...");

        for (Observer observer : observers) {
            observer.notifyObserver();
        }
    }
}

interface Observer {
    void notifyObserver();
}

class Customer implements Observer {
    private String name;

    public Customer(String name) {
        this.name = name;
    }

    @Override
    public void notifyObserver() {
        System.out.println("Customer " + name + ": Received notification about order status.");
    }
}
