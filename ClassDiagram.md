```plantuml
@startuml
class Product {
  +Prepare()
}

class ConcreteProduct {
  +Prepare()
}

class ProductFactory {
  +CreateProduct(): Product
}

class ConcreteProductFactory {
  +CreateProduct(): Product
}

class DeliveryDecorator {
  -product: Product
  +DeliveryDecorator(product: Product)
  +Prepare()
  +AddDeliveryAddress(address: string)
}

class ConcreteDeliveryDecorator {
  +ConcreteDeliveryDecorator(product: Product)
  +AddDeliveryAddress(address: string)
}

class Order {
  -observers: List<IObserver>
  +RegisterObserver(observer: IObserver)
  +PlaceOrder()
}

interface IObserver {
  +Notify()
}

class ConcreteObserver {
  +Notify()
}

Product <|-- ConcreteProduct
ProductFactory <|-- ConcreteProductFactory
Product <|-- DeliveryDecorator
DeliveryDecorator <|-- ConcreteDeliveryDecorator
Order *-- IObserver
ConcreteObserver --|> IObserver
@enduml
```
