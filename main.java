import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        
        ProductFactory factory1 = new ConcreteProductFactory();
        ProductFactory factory2 = new ConcreteProductFactory();

        Product product1 = factory1.createProduct();
        Product product2 = factory2.createProduct();

        DeliveryDecorator decoratedProduct1 = new ConcreteDeliveryDecorator(product1);
        DeliveryDecorator decoratedProduct2 = new ConcreteDeliveryDecorator(product2);

        decoratedProduct1.addDeliveryAddress("Адрес 1");
        decoratedProduct2.addDeliveryAddress("Адрес 2");

        decoratedProduct1.prepare();
        decoratedProduct2.prepare();

        Order order = new Order();
        IObserver observer1 = new ConcreteObserver();
        IObserver observer2 = new ConcreteObserver();
        order.registerObserver(observer1);
        order.registerObserver(observer2);

        order.placeOrder();
    }
}

abstract class Product {
    public void prepare() {
        System.out.println("Product is being prepared...");
    }
}

class ConcreteProduct extends Product {
    @Override
    public void prepare() {
        System.out.println("ConcreteProduct is being prepared...");
    }
}

abstract class ProductFactory {
    public abstract Product createProduct();
}

class ConcreteProductFactory extends ProductFactory {
    @Override
    public Product createProduct() {
        return new ConcreteProduct();
    }
}

abstract class DeliveryDecorator extends Product {
    private Product product;

    public DeliveryDecorator(Product product) {
        this.product = product;
    }

    @Override
    public void prepare() {
        product.prepare();
    }

    public void addDeliveryAddress(String address) {
        System.out.println("Adding delivery address: " + address);
    }
}

class ConcreteDeliveryDecorator extends DeliveryDecorator {
    public ConcreteDeliveryDecorator(Product product) {
        super(product);
    }

    @Override
    public void prepare() {
        super.prepare();
        System.out.println("ConcreteDeliveryDecorator is being prepared...");
    }
}

class Order {
    private List<IObserver> observers;

    public Order() {
        observers = new ArrayList<>();
    }

    public void registerObserver(IObserver observer) {
        observers.add(observer);
    }

    public void placeOrder() {
        System.out.println("Placing order...");

        for (IObserver observer : observers) {
            observer.notifyObserver();
        }
    }
}

interface IObserver {
    void notifyObserver();
}

class ConcreteObserver implements IObserver {
    @Override
    public void notifyObserver() {
        System.out.println("Received notification about order status.");
    }
}
